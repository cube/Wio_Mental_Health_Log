# README #
Use the Seeed Wio Terminal as a quick, simple, mood logger to better understand and track mental health over a longer period of time.

This is a prototype for an old idea I had for better understanding what influences low moods - time of day, season, and tiredness.

### Features ###
- Select GOOD, OK, BAD using the top three buttons
- Confirmation that the selection has been logged
- Access tools menu using direction press
- From the menu, view the log (still only in it's raw form), view RTC reading, adjust RTC reading from DATE and TIME macros, and delete the log file

### Todo ###
- Pagination on the raw log file viewer
- Custom emotions
- Randomized inspirational messages on the confirmation screen
- Log screen visualized data, by day, week, month, year (scroll through using left and right)
- Scrolling through the homescreen for mental health tips - how season and time of day can affect mood, and what to do about it

### Requirements ###
- An SD card inserted into the Wio
- (Optional) Battery chassis for portability

### Known Issues ###
- If the terminal lacks power, the RTC will be reset the next time it is booted
- If there are more logs than can fit on the screen on the view log file page, they will not be shown
- Adjust RTC uses the __DATE__ and __TIME__ macros, which are set at compile time. This means that the option is only useful if the program has only just been compiled. This is still the best way of setting the RTC without a permanent power source.