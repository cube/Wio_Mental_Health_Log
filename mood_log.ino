#include "TFT_eSPI.h"
#include <SPI.h>
#include <Seeed_FS.h>
#include "SD/Seeed_SD.h"
#include "RTC_SAMD51.h"
#include "DateTime.h"

TFT_eSPI tft;
RTC_SAMD51 rtc;

void home_screen()
{
  clear_screen();

  // Draw Mood Choices
  tft.setTextColor(TFT_BLACK);
  tft.fillCircle(50, 40, 30, TFT_GREEN);
  tft.drawString("Good", 25, 30);
  tft.fillCircle(120, 40, 30, TFT_YELLOW);
  tft.drawString("OK", 105, 30);
  tft.fillCircle(190, 40, 30, TFT_RED);
  tft.drawString("Bad", 170, 30);

  // Calculate Greeting + Print Instructions
  tft.setTextColor(TFT_WHITE);
  DateTime now = rtc.now();
  String greeting;
  if(now.hour() > 5 && now.hour() < 12){greeting = "Good Morning";}
  else if(now.hour() >= 12 && now.hour() < 17){greeting = "Good Afternoon";}
  else if(now.hour() >= 17 && now.hour() < 23){greeting = "Good Evening";}
  else{greeting = "Trouble sleeping?";}
  tft.drawString(greeting, 10, 100);
  tft.drawString("How are you feeling?", 10, 120);
  tft.drawString("Press the corresponding button on", 10, 150);
  tft.drawString("the top of the device to choose.", 10, 170);

  // RTC warning
  if(now.year() == 2000)
  {
    tft.setTextColor(TFT_RED);
    tft.drawString("Your device RTC may need setting", 10, 200);
  }

  // Instruction
  tft.setTextColor(TFT_BLUE);
  tft.drawString("Press directional pad for the menu.", 10, 220);
}

void seasons()
{
  clear_screen();

  DateTime now = rtc.now();

  tft.setFreeFont(&FreeSansBold9pt7b);
  tft.drawString("Seasons", 10, 10);

  tft.setFreeFont(&FreeSans9pt7b);
  tft.drawString("The seasons can affect your mood", 10, 30);
  tft.drawString("In the winter, the days are shorter,", 10, 50);
  tft.drawString("which limits how much sunlight seen.", 10, 70);
  tft.drawString("This can make us feel more lethargic.", 10, 90);
  tft.setTextColor(TFT_BLUE);
  tft.drawString("Source:", 10, 200);
  tft.drawString("https://maglit.me/coluchvible", 10, 220);

  delay(1000);
  while(1)
  {
    if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      menu(6);
      break;
    }
  }
}

void menu(int selected)
{
  clear_screen();

  char *option1;
  char *option2;
  char *option3;
  char *option4;
  char *option5;
  char *option6;

  switch(selected)
  {
    case 1:
      option1 = "> Exit";
      option2 = "View Log";
      option3 = "View RTC";
      option4 = "Adjust RTC";
      option5 = "Delete Log";
      option6 = "Seasons";
      break;
    case 2:
      option1 = "Exit";
      option2 = "> View Log";
      option3 = "View RTC";
      option4 = "Adjust RTC";
      option5 = "Delete Log";
      option6 = "Seasons";
      break;
    case 3:
      option1 = "Exit";
      option2 = "View Log";
      option3 = "> View RTC";
      option4 = "Adjust RTC";
      option5 = "Delete Log";
      option6 = "Seasons";
      break;
    case 4:
      option1 = "Exit";
      option2 = "View Log";
      option3 = "View RTC";
      option4 = "> Adjust RTC";
      option5 = "Delete Log";
      option6 = "Seasons";
      break;
    case 5:
      option1 = "Exit";
      option2 = "View Log";
      option3 = "View RTC";
      option4 = "Adjust RTC";
      option5 = "> Delete Log";
      option6 = "Seasons";
      break;
    case 6:
      option1 = "Exit";
      option2 = "View Log";
      option3 = "View RTC";
      option4 = "Adjust RTC";
      option5 = "Delete Log";
      option6 = "> Seasons";
      break;
  }

  tft.setFreeFont(&FreeSansBold9pt7b);
  tft.drawString("Tools", 10, 10);

  tft.setFreeFont(&FreeSans9pt7b);
  tft.drawString(option1, 10, 30);
  tft.drawString(option2, 10, 50);
  tft.drawString(option3, 10, 70);
  tft.drawString(option4, 10, 90);
  tft.drawString(option5, 10, 110);

  tft.setFreeFont(&FreeSansBold9pt7b);
  tft.drawString("Support", 10, 150);

  tft.setFreeFont(&FreeSans9pt7b);
  tft.drawString(option6, 10, 170);


  delay(200);
  while(1)
  {
    if(digitalRead(WIO_5S_UP) == LOW && selected > 1)
    {
      menu(selected-1);
      break;
    }
    else if(digitalRead(WIO_5S_DOWN) == LOW && selected < 6)
    {
      menu(selected+1);
      break;
    }
    else if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      switch(selected)
      {
        case 1:
          home_screen();
          delay(500);
          break;
        case 2:
          view_log();
          delay(500);
          break;
        case 3:
          view_rtc();
          delay(500);
          break;
        case 4:
          adjust_rtc();
          delay(500);
          break;
        case 5:
          delete_log();
          delay(500);
          break;
        case 6:
          seasons();
          delay(500);
          break;
      }
      break;
    }
  }  
}

void view_log()
{
  clear_screen();

  int ypos = 10;
  File file = SD.open("mood_log.txt", FILE_READ);
  if(file)
  {
    while(file.available() && ypos < 220)
    {
      String line = file.readStringUntil('\n');
      tft.drawString(line, 10, ypos);
      ypos += 20;
    }
  }

  tft.setTextColor(TFT_BLUE);
  tft.drawString("Press direction key to go back", 10, 220);

  delay(1000);
  while(1)
  {
    if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      menu(1);
      break;
    }
  }
}

void adjust_rtc()
{
  clear_screen();

  tft.setTextColor(TFT_GREEN);
  tft.drawString("Adjusting RTC...", 10, 10);
  tft.setTextColor(TFT_WHITE);
  tft.drawString(F(__DATE__), 10, 50);
  tft.drawString(F(__TIME__), 10, 70);

  DateTime now = DateTime(F(__DATE__), F(__TIME__));
  rtc.adjust(now);

  tft.setTextColor(TFT_GREEN);
  tft.drawString("Done! Press keypad to go back.", 10, 110);

  delay(1000);
  while(1)
  {
    if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      menu(1);
      break;
    }
  }
}

void view_rtc()
{
  clear_screen();

  DateTime now = rtc.now();

  tft.setTextColor(TFT_WHITE);
  tft.drawString(now.timestamp(), 10, 10);

  tft.setTextColor(TFT_BLUE);
  tft.drawString("Press keypad to go back.", 10, 220);

  delay(1000);
  while(1)
  {
    if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      menu(1);
      break;
    }
  }
}

void delete_log()
{
  clear_screen();

  tft.setTextColor(TFT_GREEN);
  tft.drawString("Deleting logfile...", 10, 10);

  SD.remove("mood_log.txt");

  tft.setTextColor(TFT_GREEN);
  tft.drawString("Done! Press keypad to go back.", 10, 110);

  delay(1000);
  while(1)
  {
    if(digitalRead(WIO_5S_PRESS) == LOW)
    {
      menu(1);
      break;
    }
  }
}



void choice_good()
{
  clear_screen();
  thank_you();

  tft.drawString("I'm glad you're feeling good right now!", 10, 50);
  tft.drawString("Hold onto that feeling, and savour it.", 10, 70);

  tft.drawString("Feel free to switch off", 10, 130);
  tft.drawString("the device now :)", 10, 150);

  DateTime now = rtc.now();
  File file = SD.open("mood_log.txt", FILE_APPEND);
  file.print(now.hour());
  file.print(":");
  file.print(now.minute());
  file.print(",");
  file.print(now.day());
  file.print("/");
  file.print(now.month());
  file.print("/");
  file.print(now.year());
  file.println(",good");
  file.close();

  while(1);
}

void choice_ok()
{
  clear_screen();
  thank_you();

  tft.drawString("Life isn't always good or bad...", 10, 50);
  tft.drawString("It's good to take time to acknowledge", 10, 70);
  tft.drawString("our feelings.", 10, 90);

  tft.drawString("Feel free to switch off", 10, 130);
  tft.drawString("the device now :)", 10, 150);

  DateTime now = rtc.now();
  File file = SD.open("mood_log.txt", FILE_APPEND);
  file.print(now.hour());
  file.print(":");
  file.print(now.minute());
  file.print(",");
  file.print(now.day());
  file.print("/");
  file.print(now.month());
  file.print("/");
  file.print(now.year());
  file.println(",ok");
  file.close();

  while(1);
}

void choice_bad()
{
  clear_screen();
  thank_you();
  
  tft.drawString("I'm sorry you don't feel", 10, 50);
  tft.drawString("so good today. This will pass", 10, 70);
  tft.drawString("and soon the skies will clear.", 10, 90);

  tft.drawString("Feel free to switch off", 10, 130);
  tft.drawString("the device now :)", 10, 150);

  DateTime now = rtc.now();
  File file = SD.open("mood_log.txt", FILE_APPEND);
  file.print(now.hour());
  file.print(":");
  file.print(now.minute());
  file.print(",");
  file.print(now.day());
  file.print("/");
  file.print(now.month());
  file.print("/");
  file.print(now.year());
  file.println(",bad");
  file.close();

  while(1);
}

void thank_you()
{
  tft.drawString("Thank you for recording your", 10, 10);
  tft.drawString("feelings today!", 10, 30);
}



void setup() {
  while(!SD.begin(SDCARD_SS_PIN, SDCARD_SPI));
  rtc.begin();
  tft.begin();
  //Serial.begin(115200);
  //while(!Serial);

  pinMode(WIO_KEY_A, INPUT_PULLUP);
  pinMode(WIO_KEY_B, INPUT_PULLUP);
  pinMode(WIO_KEY_C, INPUT_PULLUP);
  pinMode(WIO_5S_PRESS, INPUT_PULLUP);
  pinMode(WIO_5S_UP, INPUT_PULLUP);
  pinMode(WIO_5S_DOWN, INPUT_PULLUP);
  pinMode(WIO_5S_LEFT, INPUT_PULLUP);
  pinMode(WIO_5S_RIGHT, INPUT_PULLUP);

  tft.setFreeFont(&FreeSans9pt7b);
  tft.setRotation(3);

  home_screen();
}

void loop() {
  if(digitalRead(WIO_KEY_A) == LOW)
  {
    choice_bad();
  }
  else if(digitalRead(WIO_KEY_B) == LOW)
  {
    choice_ok();
  }
  else if(digitalRead(WIO_KEY_C) == LOW)
  {
    choice_good();
  }
  else if(digitalRead(WIO_5S_PRESS) == LOW)
  {
    menu(1);
    delay(1000);
  }
}



// Helper Funcs
void clear_screen()
{
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE);
}